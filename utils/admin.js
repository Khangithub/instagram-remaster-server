// firebase firestore configuration
const admin = require('firebase-admin');

const config = require('./config');

const firebaseApp = admin.initializeApp(config);

const db = firebaseApp.firestore();
const storage = firebaseApp.storage();

module.exports = {firebaseApp, db, storage};
