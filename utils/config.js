const admin = require('firebase-admin');
var serviceAccount = require('./serviceAccount.json');

module.exports = {
  credential: admin.credential.cert(serviceAccount),
  databaseURL: 'https://facebook-clone-2a127.firebaseio.com',
};
