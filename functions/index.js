const {ApolloServer, gql} = require('apollo-server-express');
const fs = require('fs');
const functions = require('firebase-functions');
const bodyParser = require('body-parser');
const cors = require('cors');
const express = require('express');
const db = require('./db');

const app = express();
app.use(cors(), bodyParser.json());

const typeDefs = gql(fs.readFileSync('./schema.graphql', {encoding: 'utf8'}));

const resolvers = require('./resolvers');

const apolloServer = new ApolloServer({
  typeDefs,
  resolvers,
});

apolloServer.applyMiddleware({app, path: '/graphql'});

exports.api = functions.region('asia-east2').https.onRequest(app);
