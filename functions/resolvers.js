const db = require('./db');

const Query = {
  post: (root, args) => db.posts.get(args.id),
  posts: () => db.posts.list(),
  notifications: () => db.notifications.list(),
};

const Mutation = {
  createPost: (
    root,
    {input: {inputUploadedMediaList, caption, displayName, uid, avatar}}
  ) => {
    console.log(
      {inputUploadedMediaList, caption, displayName, uid, avatar},
      'input createPost'
    );
    let uploadedMediaList = inputUploadedMediaList.map(
      ({inputMediaUrl, inputType}) => ({
        mediaUrl: inputMediaUrl,
        type: inputType,
      })
    );

    const id = db.posts.create({
      caption,
      commentList: [],
      createdAt: '10:59 AM 3/18/2021',
      likeCount: Math.floor(Math.random() * Math.floor(10000000)),
      displayName,
      peopleLikedPostList: [],
      uid,
      uploadedMediaList,
      avatar,
    });

    return db.posts.get(id);
  },

  likePost: async (root, {input: {id, displayName, avatar, uid}}) => {
    const {peopleLikedPostList, ...restProps} = db.posts.get(id);
    console.log({id, displayName, avatar, uid}, 'input likePost');
    await db.posts.update({
      id,
      peopleLikedPostList: [
        {
          uid,
          avatar,
          displayName,
        },
        ...peopleLikedPostList,
      ],
      ...restProps,
    });

    return await db.posts.get(id);
  },

  unlikePost: async (root, {input: {id, index}}) => {
    console.log({id, index}, 'input unlike');
    const {peopleLikedPostList, ...restProps} = db.posts.get(id);

    await db.posts.update({
      id,
      peopleLikedPostList: peopleLikedPostList.slice(index, 1),
      ...restProps,
    });

    return await db.posts.get(id);
  },
};

const Notification = {
  post: (notification) => db.posts.get(notification.postId),
};

module.exports = {Query, Mutation, Notification};
