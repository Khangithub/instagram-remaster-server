const { DataStore } = require('notarealdb');

const store = new DataStore('./data');

module.exports = {
  posts: store.collection('posts'),
  notifications: store.collection('notifications'),
};
