const graphql = require('graphql');
const {db} = require('../../utils/admin');
const postDB = require('./posts.json');
const mediaListDB = require('./mediaLists.json');
const _ = require('lodash');

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLSchema,
  GraphQLInt,
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
  GraphQLInputObjectType,
} = graphql;

const PostType = new GraphQLObjectType({
  name: 'Post',
  fields: () => ({
    id: {
      type: GraphQLID,
    },
    caption: {type: GraphQLString},
    createdAt: {type: GraphQLString},
    likeCount: {type: GraphQLInt},
    displayName: {type: GraphQLString},
    likedList: {
      type: new GraphQLList(GraphQLString),
    },
    uid: {
      type: GraphQLID,
    },
    uploadedMediaList: {
      type: new GraphQLList(MediaListType),
      resolve(parent, args) {
        return _.filter(mediaListDB, {postId: parent.id});
      },
    },
  }),
});

const MediaListType = new GraphQLObjectType({
  name: 'MediaList',
  fields: () => ({
    id: {
      type: GraphQLID,
    },
    mediaUrl: {type: GraphQLString},
    type: {type: GraphQLString},
    postId: {type: GraphQLID},
  }),
});

const MediaListInputType = new GraphQLInputObjectType({
  name: 'MediaListInput',
  fields: () => ({
    id: {
      type: GraphQLID,
    },
    mediaUrl: {type: GraphQLString},
    type: {type: GraphQLString},
    postId: {type: GraphQLID},
  }),
});

const Mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    createPost: {
      type: PostType,
      args: {
        id: {type: GraphQLID},
        caption: {type: new GraphQLNonNull(GraphQLString)},
        createdAt: {type: new GraphQLNonNull(GraphQLString)},
        likeCount: {type: new GraphQLNonNull(GraphQLInt)},
        displayName: {type: new GraphQLNonNull(GraphQLString)},
        likedList: {type: new GraphQLNonNull(new GraphQLList(GraphQLString))},
        uid: {type: new GraphQLNonNull(GraphQLString)},
        uploadedMediaList: {
          type: new GraphQLNonNull(new GraphQLList(MediaListInputType)),
        },
      },
      resolve(parent, args) {
        const {
          id,
          caption,
          createdAt,
          likeCount,
          displayName,
          likedList,
          uid,
          uploadedMediaList,
        } = args;

        async function storeData() {
          await db.collection('instagram__posts').doc(id).set({
            caption,
            uid,
            displayName,
            createdAt,
            likeCount,
            likedList,
          });

          await db.collection('instagram__media__list').add({
            uploadedMediaList,
          });

          return true;
        }

        if (storeData()) console.log('store done');
      },
    },
  },
});

const RootQuery = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    post: {
      type: PostType,
      args: {
        id: {type: GraphQLID},
      },
      async resolve(parent, args) {
        async function getAllPost() {
          var result = {};
          let instagramPostRef = db.collection('instagram__posts');
          let allPosts = await instagramPostRef.get();
          for (const doc of allPosts.docs) {
            result = {...doc.data(), id: doc.id};
          }
          return result;
        }

        let data = await getAllPost();
        console.log(data, 'data');

        return _.find(postDB, {id: args.id});
        // return db.collection('instagram__posts').get()
      },
    },
    posts: {
      type: new GraphQLList(PostType),
      resolve(parent, args) {
        return postDB;
      },
    },
    mediaList: {
      type: MediaListType,
      args: {
        id: {type: GraphQLID},
      },
      resolve(parent, args) {
        return _.find(mediaListDB, {id: args.id});
      },
    },
    mediaLists: {
      type: new GraphQLList(MediaListType),
      resolve(parent, args) {
        return mediaListDB;
      },
    },
  },
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
});
